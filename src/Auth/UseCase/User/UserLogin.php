<?php

namespace Nrg\Auth\UseCase\User;

use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Auth\Value\AuthData;

/**
 * Class UserLogin
 */
class UserLogin
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var CreateAuthData
     */
    private $createAuthData;

    /**
     * @param UserRepository $userRepository
     * @param CreateAuthData $createAuthData
     */
    public function __construct(UserRepository $userRepository, CreateAuthData $createAuthData)
    {
        $this->userRepository = $userRepository;
        $this->createAuthData = $createAuthData;
    }

    /**
     * @param array $data
     *
     * @return AuthData
     */
    public function execute(array $data): AuthData
    {
        return $this->createAuthData->execute(
            $this->userRepository->findByEmail($data['email'])
        );
    }
}
