<?php

namespace Nrg\Auth\UseCase\User;

use Nrg\Auth\Persistence\Abstraction\UserRepository;
use Nrg\Data\Exception\EntityNotFoundException;

/**
 * Class AreValidEmailAndPassword
 */
class AreValidEmailAndPassword
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function execute(array $data): bool
    {
        try {
            $user = $this->userRepository->findByEmail($data['email']);
        } catch (EntityNotFoundException $e) {
            return false;
        }

        return $data['password'] === $user->getPassword();
    }
}