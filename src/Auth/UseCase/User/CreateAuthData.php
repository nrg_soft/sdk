<?php

namespace Nrg\Auth\UseCase\User;

use Nrg\Auth\Abstraction\AccessControl;
use Nrg\Auth\Entity\User;
use Nrg\Auth\Value\AuthData;

/**
 * Class CreateAuthData
 */
class CreateAuthData
{
    /**
     * @var AccessControl
     */
    private $accessControl;

    /**
     * @param AccessControl $accessControl
     */
    public function __construct(AccessControl $accessControl)
    {
        $this->accessControl = $accessControl;
    }

    /**
     * @param User|null $user
     *
     * @return AuthData
     */
    public function execute(User $user = null): AuthData
    {
        return new AuthData(
            $this->accessControl->generateAccessToken($user ?? $this->accessControl->getUser()),
            $this->accessControl->generateRefreshToken($user ?? $this->accessControl->getUser())
        );
    }
}
