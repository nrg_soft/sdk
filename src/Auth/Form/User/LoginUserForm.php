<?php

namespace Nrg\Auth\Form\User;

use Nrg\Auth\Form\User\Element\EmailElement;
use Nrg\Auth\Form\User\Element\PasswordElement;
use Nrg\Auth\I18n\AuthDictionary;
use Nrg\Auth\UseCase\User\AreValidEmailAndPassword;
use Nrg\Form\Form;
use Nrg\I18n\Abstraction\Translator;

class LoginUserForm extends Form
{
    public function __construct(Translator $translator, AreValidEmailAndPassword $areValidEmailAndPassword)
    {
        parent::__construct($translator);

        $translator->addDictionary(AuthDictionary::class);

        $this
            ->addElement(new EmailElement())
            ->addElement(new PasswordElement($areValidEmailAndPassword));
    }
}
