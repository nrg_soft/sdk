<?php

namespace Nrg\Auth\Form\User\Validator;

use Nrg\Auth\UseCase\User\AreValidEmailAndPassword;
use Nrg\Form\Abstraction\AbstractValidator;
use Nrg\Form\Element;

/**
 * Class ValidEmailAndPassword.
 */
class ValidEmailAndPassword extends AbstractValidator
{
    public const CASE_INVALID_EMAIL_OR_PASSWORD = 0;

    /**
     * @var AreValidEmailAndPassword
     */
    private $areValidEmailAndPassword;

    public function __construct(AreValidEmailAndPassword $areValidEmailAndPassword)
    {
        $this->areValidEmailAndPassword = $areValidEmailAndPassword;
        $this->adjustErrorText('invalid email or password', self::CASE_INVALID_EMAIL_OR_PASSWORD);
    }

    /**
     * @param Element $element
     *
     * @return bool
     */
    public function isValid(Element $element): bool
    {
        $this->setErrorCase(self::CASE_INVALID_EMAIL_OR_PASSWORD);

        return $this->areValidEmailAndPassword->execute([
            'email' => $element->getForm()->getElement('email')->getValue(),
            'password' => $element->getValue(),
        ]);
    }
}
