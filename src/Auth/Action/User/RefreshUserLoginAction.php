<?php

namespace Nrg\Auth\Action\User;

use Nrg\Auth\UseCase\User\CreateAuthData;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Exception;

/**
 * Class RefreshUserLoginAction
 */
class RefreshUserLoginAction
{
    /**
     * @var CreateAuthData
     */
    private $createAuthData;

    /**
     * @param CreateAuthData $createAuthData
     */
    public function __construct(CreateAuthData $createAuthData)
    {
        $this->createAuthData = $createAuthData;
    }

    /**
     * @param HttpExchangeEvent $event
     *
     * @throws Exception
     */
    public function onNext(HttpExchangeEvent $event)
    {
        $event->getResponse()
            ->setStatus(new HttpStatus(HttpStatus::CREATED))
            ->setBody($this->createAuthData->execute());
    }
}
