<?php

namespace Nrg\Auth\Action\User;

use Nrg\Auth\Form\User\LoginUserForm;
use Nrg\Auth\UseCase\User\UserLogin;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpStatus;
use Exception;

/**
 * Class UserLoginAction
 */
class UserLoginAction
{
    /**
     * @var LoginUserForm
     */
    private $form;

    /**
     * @var UserLogin
     */
    private $userLogin;

    /**
     * @param LoginUserForm $form
     * @param UserLogin $userLogin
     */
    public function __construct(LoginUserForm $form, UserLogin $userLogin)
    {
        $this->form = $form;
        $this->userLogin = $userLogin;
    }

    /**
     * @param HttpExchangeEvent $event
     *
     * @throws Exception
     */
    public function onNext(HttpExchangeEvent $event)
    {
        $this->form->populate($event->getRequest()->getBodyParams());

        if ($this->form->hasErrors()) {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::UNPROCESSABLE_ENTITY))
                ->setBody($this->form->serialize());
        } else {
            $event->getResponse()
                ->setStatus(new HttpStatus(HttpStatus::CREATED))
                ->setBody($this->userLogin->execute($this->form->serialize()));
        }
    }
}
