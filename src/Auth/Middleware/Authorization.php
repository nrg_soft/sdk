<?php

namespace Nrg\Auth\Middleware;

use Nrg\Auth\Abstraction\AccessControl;
use Nrg\Http\Abstraction\ResponseEmitter;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Value\HttpRequest;
use Nrg\Http\Value\HttpResponse;
use Nrg\Http\Value\HttpStatus;

/**
 * Class Authorization
 */
class Authorization
{
    public const HEADER_NAME = 'Authorization';
    public const SCHEME_NAME = 'Bearer';
    /**
     * @var AccessControl
     */
    private $accessControl;

    /**
     * @var ResponseEmitter
     */
    private $responseEmitter;

    /**
     * @var array
     */
    private $freeAccessRoutes;

    /**
     * @var string|null
     */
    private $refreshAccessRoute;

    /**
     * @param AccessControl $accessControl
     * @param ResponseEmitter $responseEmitter
     */
    public function __construct(
        AccessControl $accessControl,
        ResponseEmitter $responseEmitter,
        array $freeAccessRoutes = [],
        string $refreshAccessRoute = null
    ) {
        $this->responseEmitter = $responseEmitter;
        $this->accessControl = $accessControl;
        $this->freeAccessRoutes = $freeAccessRoutes;
        $this->refreshAccessRoute = $refreshAccessRoute;
    }

    /**
     * @param HttpExchangeEvent $event
     */
    public function onNext(HttpExchangeEvent $event)
    {
        $request = $event->getRequest();

        if ($this->hasFreeAccess($request)) {
            return;
        }

        $header = $request->getQueryParam(self::HEADER_NAME) ?? $request->getHeaderLine(self::HEADER_NAME);

        if (empty($header)) {
            $this->emitUnauthorized($event->getResponse());
        }

        if (!$this->verifyScheme($header)) {
            $this->emitUnauthorized($event->getResponse(), 'Unsupported authorization scheme');
        }

        $token = $this->extractToken($header);

        if ($this->forRefreshAccessToken($request) ?
            !$this->accessControl->verifyRefreshToken($token) :
            !$this->accessControl->verifyAccessToken($token)
        ) {
            $this->emitUnauthorized($event->getResponse(), 'Invalid token was provided');
        }

        $this->accessControl->setToken($token);
    }

    private function forRefreshAccessToken(HttpRequest $request): bool
    {
        return null !== $this->refreshAccessRoute && $request->getUrl()->getPath() === $this->refreshAccessRoute;
    }

    private function hasFreeAccess(HttpRequest $request): bool
    {
        return in_array($request->getUrl()->getPath(), $this->freeAccessRoutes);
    }

    private function verifyScheme(string $header)
    {
        return substr($header, 0, strlen(self::SCHEME_NAME) + 1) === self::SCHEME_NAME.' ';
    }

    private function extractToken(string $header)
    {
        return substr($header, strlen(self::SCHEME_NAME) + 1);
    }

    private function emitUnauthorized(HttpResponse $response, string $reasonPhrase = null): void
    {
        $response->setStatusCode(HttpStatus::UNAUTHORIZED, $reasonPhrase);
        $this->responseEmitter->emit($response, true);
    }

    private function emitForbidden(HttpResponse $response): void
    {
        $response->setStatusCode(HttpStatus::FORBIDDEN);
        $this->responseEmitter->emit($response, true);
    }
}
