<?php

namespace Nrg\Auth\Service;

use Nrg\Auth\Abstraction\AccessControl;
use Nrg\Auth\Entity\AccessToken;
use Nrg\Auth\Entity\User;
use Nrg\Auth\UseCase\Token\DetailsToken;
use Nrg\Auth\UseCase\Token\IsTokenExist;
use Nrg\Http\UseCase\EmitResponse;
use Nrg\Http\Value\HttpRequest;
use Nrg\Http\Value\HttpResponse;
use Nrg\Http\Value\HttpStatus;
use Nrg\Http\Value\Url;
use Nrg\Utility\Abstraction\Config;
use Nrg\Utility\Abstraction\Settings;

/**
 * Class TokenBasedAccessControl
 */
class TokenBasedAccessControl implements AccessControl
{
    /**
     * @var AccessToken
     */
    private $token;

    /**
     * @var EmitResponse
     */
    private $emitResponse;

    /**
     * @var IsTokenExist
     */
    private $isTokenExist;

    /**
     * @var DetailsToken
     */
    private $detailsToken;

    /**
     * @var Config
     */
    private $config;

    /**
     * @param EmitResponse $emitResponse
     * @param IsTokenExist $isTokenExist
     * @param DetailsToken $detailsToken
     * @param Settings $settings
     */
    public function __construct(
        EmitResponse $emitResponse,
        IsTokenExist $isTokenExist,
        DetailsToken $detailsToken,
        Settings $settings
    ) {
        $this->emitResponse = $emitResponse;
        $this->isTokenExist = $isTokenExist;
        $this->detailsToken = $detailsToken;
        $this->config = $settings->getConfig(AccessControl::class);
    }

    /**
     * {@inheritdoc}
     */
    public function authorization(HttpRequest $request, HttpResponse $response): void
    {
        $tokenId = $this->extractTokenId($request);

        if (!empty($tokenId)) {
            if (!$this->isTokenExist($tokenId)) {
                $this->emitUnauthorized($response);
            }
            $this->setToken($this->getTokenById($tokenId));
        }

        if ($this->hasFreeAccess($request->getUrl())) {
            return;
        }

        if ($this->hasAuthorizedAccess($request->getUrl())) {
            return;
        }

        if (!$this->hasToken()) {
            $this->emitUnauthorized($response);
        }

        if (!$this->isAllowed($request->getUrl())) {
            $this->emitForbidden($response);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function hasToken(): bool
    {
        return null !== $this->token;
    }

    /**
     * {@inheritdoc}
     */
    public function getToken(): AccessToken
    {
        return $this->token;
    }

    /**
     * {@inheritdoc}
     */
    public function getUser(): User
    {
        return $this->getToken()->getUser();
    }

    /**
     * @param AccessToken $token
     */
    private function setToken(AccessToken $token): void
    {
        $this->token = $token;
    }

    /**
     * @param HttpRequest $request
     *
     * @return string
     */
    private function extractTokenId(HttpRequest $request): string
    {
        return $request->getQueryParam(self::HEADER_NAME) ?? $request->getHeaderLine(self::HEADER_NAME);
    }

    /**
     * @param Url $url
     *
     * @return bool
     */
    private function isAllowed(Url $url): bool
    {
        return $this->token->getUser()->isAllowed($url);
    }

    /**
     * @param Url $url
     *
     * @return bool
     */
    private function hasFreeAccess(Url $url): bool
    {
        return in_array(
            $url->getPath(),
            $this->config->get('freeAccessRoutes', [])
        );
    }

    /**
     * @param Url $url
     *
     * @return bool
     */
    private function hasAuthorizedAccess(Url $url): bool
    {
        return $this->hasToken() && in_array(
            $url->getPath(),
            $this->config->get('authorizedOnlyRoutes', [])
        );
    }

    /**
     * @param string $id
     *
     * @return bool
     */
    private function isTokenExist(string $id): bool
    {
        return $this->isTokenExist->execute(['id' => $id]);
    }

    /**
     * @param string $id
     *
     * @return AccessToken
     */
    private function getTokenById(string $id)
    {
        return $this->detailsToken->execute(['id' => $id]);
    }

    /**
     * @param HttpResponse $response
     */
    private function emitUnauthorized(HttpResponse $response): void
    {
        $response->setStatus(new HttpStatus(HttpStatus::UNAUTHORIZED));
        $this->emitResponse->execute($response, true);
    }

    /**
     * @param HttpResponse $response
     */
    private function emitForbidden(HttpResponse $response): void
    {
        $response->setStatus(new HttpStatus(HttpStatus::FORBIDDEN));
        $this->emitResponse->execute($response, true);
    }
}