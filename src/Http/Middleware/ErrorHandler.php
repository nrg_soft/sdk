<?php

namespace Nrg\Http\Middleware;

use DomainException;
use Nrg\Http\Event\HttpExchangeEvent;
use Nrg\Http\Exception\HttpException;
use Nrg\Http\Value\ErrorMessage;
use Nrg\Http\Value\HttpStatus;
use Nrg\Utility\Abstraction\Config;
use Throwable;

/**
 * Class ErrorHandler
 */
class ErrorHandler
{
    /**
     * @var string
     */
    private $mode;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->mode = $config->get('mode');
    }

    /**
     * @param Throwable $throwable
     * @param HttpExchangeEvent $event
     */
    public function onError(Throwable $throwable, HttpExchangeEvent $event)
    {
        if ($throwable instanceof HttpException) {
            $statusCode = $throwable->getCode();
        } elseif ($throwable instanceof DomainException) {
            $statusCode = HttpStatus::BAD_REQUEST;
        } else {
            $statusCode = HttpStatus::INTERNAL_SERVER_ERROR;
        }

        $event->getResponse()
            ->setStatusCode($statusCode)
            ->setBody(new ErrorMessage($throwable, $statusCode, 'development' === $this->mode));
    }
}
