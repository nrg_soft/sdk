<?php

namespace Nrg\Http\Value;

use JsonSerializable;
use Throwable;

/**
 * Class ErrorMessage
 */
class ErrorMessage implements JsonSerializable
{
    /**
     * @var Throwable
     */
    private $throwable;

    /**
     * @var bool
     */
    private $debug;
    /**
     * @var int
     */
    private $statusCode;

    /**
     * @param Throwable $throwable
     * @param int $statusCode
     * @param bool $debug
     */
    public function __construct(Throwable $throwable, int $statusCode, bool $debug = false)
    {
        $this->throwable = $throwable;
        $this->statusCode = $statusCode;
        $this->debug = $debug;
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize(): array
    {
        return $this->debug ? [
            'code' => $this->statusCode,
            'file' => $this->throwable->getFile(),
            'line' => $this->throwable->getLine(),
            'message' => $this->throwable->getMessage(),
        ] : [
            'code' => $this->statusCode,
            'message' => $this->throwable->getMessage(),
        ];
    }
}