<?php

namespace Nrg\Doctrine\Connection;

use Doctrine\Common\EventManager;
use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection as DbalConnection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\PDOSqlite\Driver;
use Nrg\Doctrine\Abstraction\Connection;
use Nrg\Utility\Abstraction\Settings;

class SqliteConnection extends DbalConnection implements Connection
{
    /**
     * @param Settings $settings
     * @param Configuration|null $dbalConfig
     * @param EventManager|null $eventManager
     *
     * @throws DBALException
     */
    public function __construct(Settings $settings, Configuration $dbalConfig = null, EventManager $eventManager = null)
    {
        $params = $settings->getConfig(Connection::class)->asArray() + ['driver' => 'pdo_sqlite'];

        parent::__construct($params, new Driver(), $dbalConfig, $eventManager);
    }
}
