<?php

namespace Nrg\Data\Abstraction;

use Nrg\Doctrine\Abstraction\Connection;
use Nrg\Utility\Abstraction\Settings;
use PDO;

/**
 * Class AbstractionSchema.
 */
abstract class AbstractSchema
{
    private const SCHEMA_SEPARATOR = '.';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var SchemaAdapter
     */
    private $adapter;

    /**
     * @var string
     */
    private $name;

    /**
     * @param Connection $connection
     * @param SchemaAdapter $adapter
     * @param Settings $settings
     */
    public function __construct(Connection $connection, SchemaAdapter $adapter, Settings $settings)
    {
        $this->connection = $connection;
        $this->adapter = $adapter;
        $this->name = $settings->getConfig(Connection::class)->get('schemaName');
    }

    /**
     * @return array
     */
    public function getFieldTypes(): array
    {
        return [];
    }

    /**
     * @param string $fieldName
     *
     * @return int
     */
    public function getFieldType(string $fieldName): int
    {
        return $this->getFieldTypes()[$fieldName] ?? PDO::PARAM_STR;
    }

    /**
     * @return array
     */
    abstract public function getFieldNames(): array;

    /**
     * @return string
     */
    public function getFullTableName(): string
    {
        return null === $this->name ? $this->getTableName() : $this->name.self::SCHEMA_SEPARATOR.$this->getTableName();
    }

    /**
     * @return Connection
     */
    public function getConnection(): Connection
    {
        return $this->connection;
    }

    /**
     * @return SchemaAdapter
     */
    public function getAdapter(): SchemaAdapter
    {
        return $this->adapter;
    }

    /**
     * @return string
     */
    abstract protected function getTableName(): string;
}
