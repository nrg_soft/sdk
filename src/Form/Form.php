<?php

namespace Nrg\Form;

use Nrg\Form\I18n\FormDictionary;
use Nrg\I18n\Abstraction\Translator;
use Nrg\I18n\Service\TranslatorAbility;

/**
 * Class Form.
 */
class Form
{
    use TranslatorAbility;

    /**
     * @var Element[]
     */
    public $elements = [];

    /**
     * @var bool
     */
    private $hasErrorsCache;

    /**
     * Form constructor.
     *
     * @param null|Translator $translator
     */
    public function __construct(Translator $translator = null)
    {
        $this->translator = $translator;
        $this->addDictionary(FormDictionary::class);
    }

    /**
     * @param Element $element
     *
     * @return Form
     */
    public function addElement(Element $element): self
    {
        $this->elements[$element->getName()] = $element;
        $element->setForm($this);

        return $this;
    }

    public function getElement(string $name): Element
    {
        return $this->elements[$name];
    }

    public function hasElement(string $name): bool
    {
        return isset($this->elements[$name]);
    }

    public function populate(array $data): self
    {
        foreach ($data as $name => $item) {
            if (isset($this->elements[$name])) {
                $this->elements[$name]->setValue($data[$name]);
            }
        }

        return $this;
    }

    /**
     * @param bool $useCache
     *
     * @return bool
     */
    public function hasErrors($useCache = true): bool
    {
        if ($useCache && null !== $this->hasErrorsCache) {
            return $this->hasErrorsCache;
        }

        foreach ($this->elements as $element) {
            if ($element->hasError()) {
                $this->hasErrorsCache = true;

                return true;
            }
        }
        $this->hasErrorsCache = false;

        return false;
    }

    public function serialize($useCache = true): array
    {
        $data = [];
        if ($this->hasErrors($useCache)) {
            foreach ($this->elements as $element) {
                if ($element->hasError($useCache)) {
                    $data[$element->getName()] = $this->t($element->getErrorMessage());
                }
            }
        } else {
            foreach ($this->elements as $element) {
                if ($element->hasValue()) {
                    $data[$element->getName()] = $element->getValue();
                }
            }
        }

        return $data;
    }
}
