<?php

namespace Nrg\Utility\Abstraction;

use JsonSerializable;

/**
 * Interface Config.
 */
interface Config extends JsonSerializable
{
    /**
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool;

    /**
     * @param string $key
     * @param null $defaultValue
     *
     * @return mixed
     */
    public function get(string $key, $defaultValue = null);

    /**
     * @return array
     */
    public function asArray(): array;

    /**
     * @return array
     */
    public function getPublic(): array;
}
