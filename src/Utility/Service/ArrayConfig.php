<?php

namespace Nrg\Utility\Service;

use Nrg\Utility\Abstraction\Config;

/**
 * Class ArrayConfig.
 *
 * Array configuration implementation.
 */
class ArrayConfig implements Config
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var array
     */
    private $publicKeys;


    /**
     * @param array $config
     * @param array $publicKeys
     */
    public function __construct(array $config, array $publicKeys = [])
    {
        $this->config = $config;
        $this->publicKeys = $publicKeys;
    }

    /**
     * @param string $key
     *
     * @return bool
     */
    public function has(string $key): bool
    {
        return isset($this->config[$key]);
    }

    /**
     * {@inheritdoc}
     */
    public function get(string $key, $defaultValue = null)
    {
        return $this->config[$key] ?? $defaultValue;
    }

    /**
     * {@inheritdoc}
     */
    public function asArray(): array
    {
        return (array)$this->config;
    }

    /**
     * @return array
     */
    public function getPublic(): array
    {
        return array_intersect_key($this->asArray(), array_flip($this->publicKeys));
    }

    /**
     * {@inheritdoc}
     */
    public function jsonSerialize()
    {
        return $this->getPublic();
    }
}
