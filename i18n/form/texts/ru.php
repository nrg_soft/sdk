<?php

return [
    'please provide a correct value' => 'пожалуйста, укажите корректное значение',
    'please fill out this field' => 'пожалуйста, заполните это поле',
    'please provide a valid e-mail' => 'пожалуйста, укажите корректный e-mail',
    'please provide a valid string' => 'пожалуйста, укажите корректную строку',
    'the length must be greater or equal to %d' => 'длина должна быть больше или равна %d',
    'the length must be less or equal to %d' => 'длина должна быть меньше или равна %d',
    'please provide a valid uuid' => 'пожалуйста, укажите корректный uuid',
    'please provide a valid array' => 'пожалуйста, укажите корректный массив',
    'the element with key \'%s\' is invalid' => 'элемент с ключем \'%s\' не валиден',
    'the element must be in: \'%s\'' => 'элемент может быть равен: \'%s\'',
];
